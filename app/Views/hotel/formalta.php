<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <div class="container-fluid">
            <h1 class="text-primary"><?= $title?></h1>
            <form action="<?= site_url('')?>" method="post">
                
                <div class="form-group">
                    <label for="id">Id </label>
                    <input type="text" name="id" value="" id="id" class="form-control"/>
                </div>
                
                <div class="form-group">
                    <label for="adultos">Adultos </label>
                    <input type="text" name="adultos" value="" id="adultos" class="form-control"/>
                </div>
                
                <div class="form-group">
                    <label for="capacidad">Capacidad </label>
                    <input type="text" name="capacidad" value="" id="capacidad" class="form-control"/>
                </div>
                
                <div class="form-group">
                    <label for="nombre">Nombre </label>
                    <input type="text" name="nombre" value="" id="nombre" class="form-control"/>
                </div>
                
                <div class="form-group">
                    <label for="tecnologia">Tecnología </label>
                    <input type="text" name="tecnologia" value="" id="tecnologia" class="form-control"/>
                </div>
                    
        </div>
    </body>
</html>

