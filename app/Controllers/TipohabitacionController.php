<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\TipohabitacionModel;

/**
 * Description of TipohabitacionController
 *
 * @author Eneko
 */
class TipohabitacionController extends BaseController {
    
public function habitaciones() {

       /* $data['title'] = 'Formulario habitaciones';
        return view('hotel/formalta', $data); */
    
    /* PARTE 1, ESTO FUNCIONA
     * 
        $tipohabitacionModel = new TipohabitacionModel();
        echo '<pre>';
        print_r($tipohabitacionModel->findAll());
        echo '</pre>';
    */ 
        
        /*$data['title'] = 'Listado de habitaciones';
        $data['habitaciones'] = $TipohabitacionModel->findAll();*/
    
    /* PARTE 2, PARA VER EL FORMULARIO
     * 
         $data['title'] = 'Formulario Habitaciones';
         return view('hotel/formalta', $data);
    */ 
    
        $tipohabitacionModel = new TipohabitacionModel();
        $data['title'] = 'Listado de habitaciones';
        $data['habitaciones'] = $tipohabitacionModel->findAll();
        return view('hotel/formalta', $data);
         
}
   
}
